import datetime
import authtools
from django.db import models
from django.conf import settings
from django.utils import timezone

# hardcoded in for now;
# if more bins are desired,
# one could change the data migration and these constants.
NEVER_BIN_NUMBER = 11
DIFFICULT_BIN_NUMBER = 12

class Card(models.Model):
    word = models.TextField()
    definition = models.TextField()

    def __unicode__(self):
        return u"Card: {}".format(self.word)

class Bin(models.Model):
    number = models.IntegerField()
    time_to_next_review = models.DurationField()

    def __unicode__(self):
        return u"Bin {} ({})".format(self.number, self.time_to_next_review)

class CardRecord(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    card = models.ForeignKey(Card)
    current_bin = models.ForeignKey(Bin)
    time_of_review = models.DateTimeField(blank=True, null=True)
    num_incorrect = models.IntegerField(default=0)

    def save(self, *args, **kwargs):
        if self.current_bin.number >= NEVER_BIN_NUMBER:
            self.time_of_review = None
        else:
            self.time_of_review = timezone.now() + self.current_bin.time_to_next_review
        super(CardRecord, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"Record of {} for Card {} (Bin {})".format(
            self.user,
            self.card.word,
            self.current_bin.number
        )

    class Meta:
        ordering = ['current_bin__number', 'time_of_review']

def get_card_list(user_id):
    """Returns a list of dicts:
        (card, bin_number, time_of_review, num_incorrect)
    """
    card_list = []
    existing_record_card_ids = []
    relevant_records = CardRecord.objects.filter(user_id=user_id)
    for record in CardRecord.objects.filter(user_id=user_id):
        card_list.append({
            'card': record.card,
            'bin_number': record.current_bin.number,
            'time_of_review': record.time_of_review,
            'num_incorrect': record.num_incorrect
        })
        existing_record_card_ids.append(record.card.id)
    now = timezone.now()
    for card in Card.objects.exclude(pk__in=existing_record_card_ids):
        card_list.append({
            'card': card,
            'bin_number': 0,
            'time_of_review': now,
            'num_incorrect': 0
        })
    return card_list

def process_result(record_id, correct):
    """Places the card into the correct bin for this record's user."""
    record = CardRecord.objects.get(pk=record_id)
    if correct and record.current_bin.number < NEVER_BIN_NUMBER:
        record.current_bin = Bin.objects.get(number=(record.current_bin.number+1))
    elif not correct:
        record.num_incorrect += 1
        if record.num_incorrect >= 10:
            record.current_bin = Bin.objects.get(number=DIFFICULT_BIN_NUMBER)
        elif record.current_bin.number != 1:
            record.current_bin = Bin.objects.get(number=1)
    record.save()

def get_next_record(user_id):
    """Called when getting a new card to review. Returns either
    1) a tuple (record_id, card) for the next record and its associated card,
    2) a datetime when there are cards in bins 1-10 that aren't ready yet, or
    3) None otherwise.
    """
    existing_record_card_ids = []
    nearest_time_of_review = None
    for record in CardRecord.objects.filter(user_id=user_id):
        existing_record_card_ids.append(record.card.id)
        if record.current_bin.number >= NEVER_BIN_NUMBER:
            continue
        if record.time_of_review <= timezone.now():
            return record.id, record.card
        if nearest_time_of_review is None or nearest_time_of_review > record.time_of_review:
            nearest_time_of_review = record.time_of_review
    for card in Card.objects.exclude(pk__in=existing_record_card_ids):
        record = CardRecord(
            user=authtools.models.User.objects.get(pk=user_id),
            card=card,
            current_bin=Bin.objects.get(number=0)
        )
        record.save()
        return record.id, card
    if len(existing_record_card_ids) > 0:
        return nearest_time_of_review
    return None
