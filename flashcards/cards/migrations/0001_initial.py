# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Bin',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.IntegerField()),
                ('time_to_next_review', models.DurationField()),
            ],
        ),
        migrations.CreateModel(
            name='Card',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('word', models.TextField()),
                ('definition', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='CardRecord',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time_of_review', models.DateTimeField(auto_now=True, null=True)),
                ('num_incorrect', models.IntegerField(default=0)),
                ('card', models.ForeignKey(to='cards.Card')),
                ('current_bin', models.ForeignKey(to='cards.Bin')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['current_bin__number', 'time_of_review'],
            },
        ),
    ]
