# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime

from django.db import migrations, models

BIN_TIMINGS = {
    0: datetime.timedelta(),
    1: datetime.timedelta(seconds=5),
    2: datetime.timedelta(seconds=25),
    3: datetime.timedelta(minutes=2),
    4: datetime.timedelta(minutes=10),
    5: datetime.timedelta(hours=1),
    6: datetime.timedelta(hours=5),
    7: datetime.timedelta(days=1),
    8: datetime.timedelta(days=5),
    9: datetime.timedelta(days=25),
    10: datetime.timedelta(days=120),
    # hopefully 999999999 is a reasonable substitute for "never",
    # but as of the time of writing, a check is written in
    # models.get_next_record to make sure we don't pull from one of these two.
    11: datetime.timedelta(days=999999999),
    12: datetime.timedelta(days=999999999)
}

def initialize_bins(apps, schema_editor):
    Bin = apps.get_model("cards", "Bin")
    for n in range(13):
        b = Bin(number=n, time_to_next_review=BIN_TIMINGS[n])
        b.save()

class Migration(migrations.Migration):
    dependencies = [
        ('cards', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(initialize_bins)
    ]
