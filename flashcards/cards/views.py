import datetime
from braces.views import LoginRequiredMixin, UserPassesTestMixin
from django.shortcuts import render, redirect
from django.views.generic import View, CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from .models import Card, get_card_list, get_next_record, process_result

class CardList(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        card_list = get_card_list(request.user.id)
        return render(request, "cards/card_list.html", {
            "object_list": card_list
        })

class CardCreate(LoginRequiredMixin, CreateView):
    model = Card
    fields = ['word', 'definition']
    success_url = reverse_lazy('list')

class CardUpdate(LoginRequiredMixin, UpdateView):
    model = Card
    fields = ['word', 'definition']
    success_url = reverse_lazy('list')

class CardDelete(LoginRequiredMixin, DeleteView):
    model = Card
    success_url = reverse_lazy('list')

class LearningView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        result = get_next_record(request.user.id)
        if result is None:
            return render(request, "cards/learn.html", {
                "record_id": -1,
                "card": None,
                "next": None
            })
        elif isinstance(result, datetime.datetime):
            return render(request, "cards/learn.html", {
                "record_id": -1,
                "card": None,
                "next": result
            })
        return render(request, "cards/learn.html", {
            "record_id": result[0],
            "card": result[1],
            "next": None
        })

    def post(self, request, *args, **kwargs):
        record_id = request.POST["record_id"]
        correct = request.POST["correct"]
        if correct == "false":
            correct = False
        elif correct == "true":
            correct = True
        else:
            raise Exception("Invalid input to learning form!")
        process_result(record_id, correct)
        return redirect('learn')
