from django.conf.urls import url
from . import views

urlpatterns = [
    url(
        regex=r"^$",
        view=views.CardList.as_view(),
        name="list",
    ),
    url(
        regex=r"^add/$",
        view=views.CardCreate.as_view(),
        name="add",
    ),
    url(
        regex=r"^(?P<pk>[0-9]+)/$",
        view=views.CardUpdate.as_view(),
        name="update",
    ),
    url(
        regex=r"^(?P<pk>[0-9]+)/delete/$",
        view=views.CardDelete.as_view(),
        name="delete",
    ),
    url(
        regex=r"^learn/$",
        view=views.LearningView.as_view(),
        name="learn",
    )
]
