from django.conf.urls import include, url
from django.conf import settings
from django.contrib import admin

urlpatterns = [
    url(r'^accounts/', include('authtools.urls')),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^' + settings.ADMIN_PATH + r'/', include(admin.site.urls)),
    url(r'', include('cards.urls')),
    url(r'^signup/', include('signup.urls'))
]
