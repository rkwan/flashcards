import environ

root = environ.Path(__file__) - 2
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env()

DEBUG = env.bool("FLASHCARDS_DEBUG")

SECRET_KEY = env.str("FLASHCARDS_SECRET_KEY")

ADMINS = (
    (env.str("FLASHCARDS_ADMIN_NAME"), env.str("FLASHCARDS_ADMIN_EMAIL")),
)
MANAGERS = ADMINS

ALLOWED_HOSTS = ['localhost', '127.0.0.1']
curr_host = env.str("FLASHCARDS_HOST", default='')
if curr_host:
    ALLOWED_HOSTS.append(curr_host)

# ADMIN_PATH controls where the admin urls are.
# e.g. if ADMIN_PATH == 'adminsite', then the admin site
# should be available at /adminsite/ instead of /admin/.
ADMIN_PATH = env.str("FLASHCARDS_ADMIN_PATH")

BASE_DIR = root()
MEDIA_ROOT = root("media")
MEDIA_URL = '/media/'
STATIC_ROOT = root("static")
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    root("assets"),
)
LOGIN_REDIRECT_URL = '/'

SERVER_EMAIL = env.str("FLASHCARDS_EMAIL_USER")
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = env.str("FLASHCARDS_EMAIL_USER")
EMAIL_HOST_PASSWORD = env.str("FLASHCARDS_EMAIL_PASSWORD")
EMAIL_PORT = 587

DATABASES = {'default': env.db("FLASHCARDS_POSTGRES_URL")}

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"

DJANGO_CORE_APPS = [
    'grappelli',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.humanize',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

THIRD_PARTY_APPS = [
    'authtools',
    'crispy_forms'
]

CUSTOM_APPS = [
    'cards',
    'signup'
]

INSTALLED_APPS = DJANGO_CORE_APPS + THIRD_PARTY_APPS + CUSTOM_APPS

AUTH_USER_MODEL = 'authtools.User'

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'flashcards.urls'

WSGI_APPLICATION = 'flashcards.wsgi.application'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': (root("templates"),),
        'APP_DIRS': True,
        'OPTIONS': {
            'debug': DEBUG,
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

TIME_ZONE = 'America/Los_Angeles'
LANGUAGE_CODE = 'en-us'
USE_I18N = False
USE_L10N = True
USE_TZ = True

CRISPY_TEMPLATE_PACK = 'bootstrap3'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
	'null': {
	    'class': 'logging.NullHandler',
	},
    },
    'loggers': {
	'django.security.DisallowedHost': {
	    'handlers': ['null'],
	    'propagate': False,
	},
    }
}
