import authtools
from django.views.generic import FormView
from django.core.urlresolvers import reverse_lazy

class SignupForm(FormView):
    template_name = 'signup/signup_form.html'
    form_class = authtools.forms.UserCreationForm
    success_url = reverse_lazy('list')

    def form_valid(self, form):
        form.save()
        return super(SignupForm, self).form_valid(form)
