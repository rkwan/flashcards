# Heart of the Cards

## Built with...

* Language: Python 2.7 (mostly)
* Framework: Django 1.8
* Database: Postgres
* Other technologies/frameworks/etc.: Bootstrap 3

## First time setting up?

1. Install the requirements: `pip install -r requirements.txt`
2. Start up Postgres (if you haven't already).
3. `cd flashcards`
4. Create `flashcards/.env` like so:

    ```
    # if you want debug mode on
    FLASHCARDS_DEBUG=True
    # if you're deploying
    FLASHCARDS_HOST="coolflashcards.com"
    # should be a random string
    FLASHCARDS_SECRET_KEY=hello
    FLASHCARDS_ADMIN_PATH=admin
    # e.g. postgres://rkwan@localhost:5432/handshake
    FLASHCARDS_POSTGRES_URL=postgres://USER:PASS@HOST:PORT/NAME
    FLASHCARDS_ADMIN_NAME="Ronald Kwan"
    FLASHCARDS_ADMIN_EMAIL="ronaldskwan@gmail.com"
    # only works with gmail
    FLASHCARDS_EMAIL_USER="ronaldskwan@gmail.com"
    FLASHCARDS_EMAIL_PASSWORD="MYPASSWORD"
    ```

5. Run `python manage.py migrate`.
6. Run `python manage.py createsuperuser`.
7. Run `python manage.py runserver` and go to `localhost:8000/$YOUR_ADMIN_PATH_HERE/` to make sure things aren't horribly broken.
